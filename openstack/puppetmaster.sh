#!/bin/bash -v

# 
# https://docs.puppet.com/puppet/latest/reference/install_linux.html
#

#######################################################
#           Script to install puppetagent on agent's. #
#######################################################

## install puppet 
wget https://apt.puppetlabs.com/puppet6-release-bionic.deb
dpkg -i puppet6-release-bionic.deb
apt-get update
apt-get install puppet-agent -y
echo 'export PATH=$PATH:/opt/puppetlabs/bin/' >> ~/.bashrc
echo '. ~/.bashrc' >> bash.bashrc


echo "Puppet is installed" > ~/boot.txt

## install puppetserver
wget https://apt.puppetlabs.com/puppet6-release-bionic.deb
sudo dpkg -i puppet6-release-bionic.deb
sudo apt-get update
sudo apt-get install puppetserver -y
echo 'PATH=$PATH:/opt/puppetlabs/bin/' >> ~/.bashrc
. ~/.bashrc

#loop to ensure ip exists before it goes further
while :
do 
    ip="$(hostname -I)"
    subString=$(echo $ip | cut -c1-8)
    if [ "$subString" == "192.168." ]
    then 
        echo "got ip" >> ~/boot.txt
        echo "$ip""$(hostname).puppetlabs.vm $(hostname)" >> /etc/hosts
        echo "$ip""puppet" >> /etc/hosts
        
        #puppetserver ca setup
        #puppet resource service puppetserver ensure=running enable=true 
        #
        # ALSO install the agent on the server
        /opt/puppetlabs/bin/puppet config set server server1-pp.puppetlabs.vm --section main
        /opt/puppetlabs/bin/puppet config set runinterval 300 --section main
        /opt/puppetlabs/bin/puppet resource service puppet ensure=running enable=true

        echo "Puppetserver is installed" >> ~/boot.txt
        break
    fi
    echo $ip >> ~/time.txt
    date +"%H:%M:%S" >> ~/time.txt
    sleep 5
done


## clone our git repo!
cd ~
git clone https://gruppe7:tEBMFMm68eoMbqqxCVYx@gitlab.stud.idi.ntnu.no/kristo11/gruppe7.git


## copy modules to the corect folders
cp -r gruppe7/montr/nodeexp/ /etc/puppetlabs/code/environments/production/modules
cp -r gruppe7/montr/prometheus/ /etc/puppetlabs/code/environments/production/modules
. ~/.bashrc
## install archive module with dependencies 
puppet module install puppet-archive --version 6.0.2

## install modules to be installed (webserver/haproxy)
puppet module install puppetlabs-apache --version 8.3.0

puppet module install puppetlabs-haproxy --version 6.4.0

## copy puppet haproxy module.
cp gruppe7/apacheAndWeb/haproxy.pp /etc/puppetlabs/code/environments/production/modules/haproxy/manifests

## copy the correct site.pp
cp gruppe7/site.pp /etc/puppetlabs/code/environments/production/manifests/site.pp

