#!/bin/bash


#######################################################
#           Script to install puppetagent on agent's. #
#######################################################

## Download agent (from lab6v2)

tempdeb=$(mktemp /tmp/debpackage.XXXXXXXXXXXXXXXXXX) || exit 1
wget -O "$tempdeb" https://apt.puppetlabs.com/puppet6-release-bionic.deb
dpkg -i "$tempdeb"
apt-get update
apt-get -y install puppet-agent
 # add Puppet binaries to PATH:
 echo 'export PATH=$PATH:/opt/puppetlabs/bin/' >> ~/.bashrc
 # source .bashrc to apply:
 . ~/.bashrc
 
echo "Puppet is installed" > ~/boot.txt

## place priv. ip add. in /etc/hosts to enable puppet to use DNS 
echo "$(/opt/puppetlabs/bin/facter networking.ip) $(hostname).agent.vm $(hostname)" >> /etc/hosts
echo $master_IP" server1-pp.puppetlabs.vm server1-pp" >> /etc/hosts

## set default variables in puppet enviorment and start puppet service.
/opt/puppetlabs/bin/puppet config set server server1-pp.puppetlabs.vm --section main
/opt/puppetlabs/bin/puppet config set runinterval 300 --section main
/opt/puppetlabs/bin/puppet resource service puppet ensure=running enable=true


## clone our gitlab repo with files.
cd ~
git clone https://gruppe7:tEBMFMm68eoMbqqxCVYx@gitlab.stud.idi.ntnu.no/kristo11/gruppe7.git


## copy website files to /var/www
cp -r gruppe7/apacheAndWeb/website/ /var/www/



echo "Puppet is installed with manager IP" $master_IP >> ~/boot.txt
