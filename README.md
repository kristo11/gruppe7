# Infrastructure as code, group 7

Gitlab repo for project in the cource IIKG3005: Infrastrucure as code. Following this README file will give you knowledge of what this gitlab repo contains and how to run our project. 

## File structure

The filestructure of this project is fairly simple. The apacheAndWeb folder contains nesseseary files to run and start up our web application. Montr contains the requierd files to apply monitoring thrugh node_exporter and prometheus. The requierd heat code to launch our servres and it's dependencies if found under the Openstack folder. Following this readme file will get you up to speed of how to run the service. 

### Our file structure

    .
    ├── apacheAndWeb
    │   ├── haproxy.pp
    │   ├── HOWTO.md
    │   └── website
    │       ├── index.html
    │       └── styles.css
    ├── montr
    │   ├── nodeexp
    │   │   ├── manifests
    │   │   │   ├── init.pp
    │   │   │   └── nodeexparchive.pp
    │   │   └── README.md
    │   └── prometheus
    │       └── manifests
    │           ├── init.pp
    │           └── prometheusarchive.pp
    ├── openstack
    │   ├── heat_project_env.yaml
    │   ├── heat_template_NewProperties.yaml
    │   ├── kristo11.pem
    │   ├── puppetagent.sh
    │   └── puppetmaster.sh
    ├── README.md
    └── site.pp


## Running the service

### Requierments 
The first requierment to run this setup is to have resources in the cloud providor openstack. We are using NTNU's private cloud to run our instances. 

In order to connect to openstack, it's requierd to have a private key uploaded in your openstack account. 
Follow [Lab 1 - Introduction to OpenStack](https://gitlab.stud.idi.ntnu.no/jhklemet/iikg3005-labs/-/blob/master/lab1.md) to set up your openstack enviorment. Note point three for creation of key. 

Note that you need to allow puppet comunication by adding a rule in the linux seciroty group. Allow tcp to flow thrugh port 8140.


## Openstack Heat
### Setup the heat
We have automated the process of creating the nesseseary parts of the stack for out program.

Start by cloning out gitlab reposotory to the preferd foler: 

    git clone https://gitlab.stud.idi.ntnu.no/kristo11/gruppe7.git

Head into the folder containting the openstack files: 
    
    cd gruppe7/openstack/


Set the name of your newly created key in the enviorment file: 
    
    nano heat_project_env.yaml  

(inside heat_project_env.yaml )
```
parameters:
    key_name: KEYNAME
```
The rest of the parameters do not need to be edited.



Remember to source your openstack acount to get access to openstack commands. Verify connection to openstack client: 

    openstack stack list

- and that your project has enugh resources (6 vcpu and 12gb ram)

### Create the stack
In order to create the stack, run this command:

    openstack stack create -t heat_template_NewProperties.yaml -e heat_project_env.yaml Gruppe7

After aprox. two minutes, verify the Stack Status
    
    openstack stack list


Congrats, you now have 5 servers up and running! 

### Set up puppetserver and puppetagents: 
List the puppet masters floating ip: 

    openstack server list | grep pp


SSH into the puppet master: 

    ssh -i KEYNAME.pem ubuntu@FloatingIP-Address


On the master, install modules we use: 

Install Archive 

    puppet module install puppet-archive --version 6.0.2

Install Apache module
    
    puppet module install puppetlabs-apache --version 8.3.0

Install haproxy module

    puppet module install puppetlabs-haproxy --version 6.4.0


Run following commands to start setup of the server/agent relationship: 

    sudo su 
    cd 
    puppetserver ca setup
	puppet resource service puppetserver ensure=running enable=true


Wait aprox. 5 minutes to let the agents request data from the master. 

List all depending sertificates to be signed, and ensure all 4 agents are listed: 
    puppetserver ca list

Sign the sertificates: 
    
    puppetserver ca sign --all


Puppetmaster/agent relationship should now be up and running. 


The next part of the README.md is related to two spesific parts of our project: Monitoring and our website (running on apache with load balancer). 

Note that these two for now is independent. 



## Monitoring
### Setup of Node exporter and Promethius

Node_exporter and prometheus will be installed on the monitor server (server2-mo) by applying it's puppet code using:

    puppet agent -t

After that the node_exporter and prometheus can be started by navigating to:

    cd ~
and from there run: (node_exporter)

    ./node_exporter/node_exporter-1.4.0.linux-amd64/node_exporter &

and (prometheus)

    ./prometheus/prometheus-2.39.1.linux-amd64/prometheus --config.file=./prometheus/prometheus-2.39.1.linux-amd64/prometheus.yml &

To see if node_exporter running correctly you can run:

    curl http://localhost:9100/metrics


To access the prometheus admin centre you must use a proxy from your browser into the server2-mo vm.
We have opted to use foxyproxy ourself, but other options can be used.

Use [this guide](https://oxylabs.io/blog/set-up-configure-foxyproxy) to set up foxyproxy in your browser. Then use SSH to create a tunnell to your VM on port 5000: 

    ssh -D 5000 -i KEYNAME.pem ubuntu@ip-address

After setting up your proxy and connecting to the vm you can visit the admin centre in your browser using:

    localhost:9090




## Web service

### Apache

This module assumes that the website is copied into this directory /var/www/website/ which in our case is implememented using git clone inside the boot script. You can also change the variable docroot in the apache class (further explanation of this underneath).

With this module it is not necessary to create an apache.pp file because we had difficulties getting this to work. Instead we have to declare a small class in site.pp within each corresponding apache node, otherwise it will create a default html directory and run this code instead of the code specified in apache::vhost. The reason for this is unclear.

### Haproxy

After the bootup script is completed, this module now contains a haproxy.pp file which contains the class declaration with default parameters. Take a five minutes break and let the servers set up the software. 

You can test that the loadbalancer and webserver is up and running by using this command from the server1-pp: 

    curl 192.168.101.212


## Conclution

You have now created 5 servers with a puppet master/agent relationship. Each agent is install with with software defined on the pupperserver. You shuld be able to view and monitor the server2 and have a scalable webiste up and running using apache and Haproxy. 
