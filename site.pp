node 'default' {

}

node 'server2-mo.agent.vm' {

        include nodeexp
        include prometheus
        notify { 'install complete': }
}

node 'server3-ap1.agent.vm' {
    class { 'apache':
  default_vhost => false,
 }
 apache::vhost { 'g7.autopoker.net':
   port    => 80,
   docroot => '/var/www', #Using website dir from puppet git module
 }
}


node 'server4-ap2.agent.vm' {
     class { 'apache':
  default_vhost => false,
 }
  apache::vhost { 'g7.autopoker.net':
   port    => 80,
   docroot => '/var/www',
 }
}
node 'server5-lb.agent.vm' {
 include ::haproxy

  haproxy::listen { 'puppet00':
    mode             => 'http',
    collect_exported  => true,
    ipaddress        => $facts['networking']['ip'],
    ports            => '80',
  }
   haproxy::balancermember { 'server00':
    listening_service => 'puppet00',
    server_names      => 'server3-ap1',
    ipaddresses       => '10.212.169.130',
    ports             => '80',
    options           => 'check',
  }
   haproxy::balancermember { 'server01':
    listening_service => 'puppet00',
    server_names      => 'server4-ap2',
    ipaddresses       => '10.212.168.180',
    ports             => '80',
    options           => 'check',
  }
}
