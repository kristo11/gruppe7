# How to set up our website with apache

### download the haproxy and apacke modules on master
https://forge.puppet.com/modules/puppetlabs/apache/
https://forge.puppet.com/modules/puppetlabs/haproxy/

### The apache.pp file is identical on both apache nodes.
#### replace path-to-sshkey and clone the git repo in the server's root dir:
sudo ssh-agent bash -c 'ssh-add /path-to-sshkey; git clone git@gitlab.stud.idi.ntnu.no:kristo11/gruppe7.git'

#### in the script replace "gruppe7n2" with whatever you like and contact me at sondrjor@stud.ntnu.no so I can enable the domain
#### use this command in ubuntu, this is probably necessary because the script was written in windows:
sed -i -e 's/\r$//' webserverScript.sh

## Databasecluster and replica with cockroachdb

### current error: cant log into cli on another node
Need to have a lower stdlib version and archive version:
puppet module install puppet-archive --version 4.2.0 --force
puppet module install puppetlabs-stdlib --version 6.6.0 --force
https://forge.puppet.com/modules/neckbeards/cockroachdb/reference
After configuring the puppet file run the command
cockroach init --insecure --host=10.212.141.247

cockroach init --insecure --host=10.212.141.247
cockroach sql --insecure
cockroach cert create-ca --certs-dir=/key --ca-key=/key.key --allow-ca-key-reuse
USE films;
CREATE TABLE films (
name STRING(50),
year INT
);
INSERT INTO films ("name", "year")                                                                   VALUES ('The departed', 2006);

cockroach cert create-ca \
 --certs-dir=\home\ubuntu \
 --ca-key=\home\ubuntu
cockroach cert create-client \
 sondre \
 --certs-dir=\key \
 --ca-key=\key