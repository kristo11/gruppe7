requires:
https://forge.puppet.com/modules/puppet/archive/readme

puppet module install puppet-archive --version 6.0.2

or 
mod 'puppet-archive', '6.0.2'
in puppet file

dependency:
https://forge.puppet.com/modules/puppetlabs/stdlib
