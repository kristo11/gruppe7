class nodeexp {

    file { '/root/node_exporter':
    ensure => 'directory',
    }

    file { '/tmp/node_exporter':
    ensure => 'directory',
    }
  include nodeexp::nodeexparchive
}
