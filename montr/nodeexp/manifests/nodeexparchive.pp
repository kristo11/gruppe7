class nodeexp::nodeexparchive {

    archive { 'node_exporter.tgz':
    path         => "/tmp/node_exporte.tgz",
    source       => "https://github.com/prometheus/node_exporter/releases/download/v1.4.0/node_exporter-1.4.0.linux-amd64.tar.gz",
    extract      => true,
    extract_path => "/root/node_exporter",
    creates      => "node_exporter-1.4.0.linux-amd64",
    cleanup      => true,
    #require      => File['~/node_exporter'],
    }

}
