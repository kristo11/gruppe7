class prometheus {

    file { '/root/prometheus':
    ensure => 'directory',
    }

    file { '/tmp/prometheus':
    ensure => 'directory',
    }
  include prometheus::prometheusarchive
}
