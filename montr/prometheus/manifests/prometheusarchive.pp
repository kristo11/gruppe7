class prometheus::prometheusarchive {

    archive { 'prometheus.tgz':
    path         => "/tmp/prometheus.tgz",
    source       => "https://github.com/prometheus/prometheus/releases/download/v2.39.1/prometheus-2.39.1.linux-amd64.tar.gz",
    extract      => true,
    extract_path => "/root/prometheus",
    creates      => "prometheus-2.39.1.linux-amd64",
    cleanup      => true,
    }

}
